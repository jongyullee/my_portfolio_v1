<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Email_lib {

	var $CI;

	public function __construct()
	{
		$this->CI = &get_instance();
	}

	public function email($param)
	{

		$this->CI->load->library('email');

		$config = array();
		$config['charset'] = 'utf-8';
		$config['protocol'] = 'mail';
		$config['wordwrap'] = true;
		$config['mailtype'] = 'html';

		$this->CI->email->initialize($config);
		$this->CI->email->from($param['email_from']);
		$this->CI->email->to($param['email_to']);
		$this->CI->email->subject($param['subject']);
		$this->CI->email->message($param['message']);
		$result = $this->CI->email->send();

		return $result;
	}

}

?>