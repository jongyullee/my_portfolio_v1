<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

		$this->load->library('email_lib');

		$lib_param = array(
			 'email_from' => $this->input->post('email'),
			 'email_to'	 => 'jongyulc@gmail.com',
			 'subject'	 => 'JLeeLab Contact - ' . $this->input->post('name'),
			 'message'	 => 'From ' . $this->input->post('email') . ', ' . $this->input->post('name') . br(2) . $this->input->post('message')
		);

		return $this->email_lib->email($lib_param);
	}

}
