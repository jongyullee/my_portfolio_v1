<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{

		$data_content = '';

		$this->template->set_template('default');
		$this->template->write('browser_title', 'PHP Web Developer in Edmonton');
		$this->template->write_view('content', 'public/home', $data_content, TRUE);
		$this->template->render();
	}

}
