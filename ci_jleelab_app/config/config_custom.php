<?php

// default browser title
$config['browser_title_base'] = ' - JLee Lab';

// template vars
$config['template_doctype'] = '<!DOCTYPE html>';
$config['template_meta_http_equiv'] = '<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>';
//$config['template_robots'] = '<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">';
$config['template_robots'] = '';
$config['template_viewport'] = '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">';
$config['template_favicon'] = '<link href="http://jleelab.com/includes/images/favicon.png" rel="shortcut icon" type="image/png" />';
