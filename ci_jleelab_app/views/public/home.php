<section id="service">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h1>Service</h1>
				<div class="subtitle-underline underline-align-center underline-orange"></div>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-lg-6">
				<?php
				$attr = array(
					 'title'	 => 'Wordpress Development',
					 'src'		 => 'includes/images/content/web.png',
					 'class'	 => 'img-responsive'
				);

				echo img($attr);
				?>
				<h3>Custom Web Development</h3>
				<p>Designing database and developing web applications based on requirements using MySQL, PHP Framework, Codeigniter, and Bootstrap.</p>
			</div>
			<div class="col-lg-6">
				<?php
				$attr = array(
					 'title'	 => 'Wordpress Development',
					 'src'		 => 'includes/images/content/wordpress.png',
					 'class'	 => 'img-responsive'
				);

				echo img($attr);
				?>
				<h3>Wordpress Development</h3>
				<p>Building Custom Wordpress theme using references mostly twelve series themes and CODEX.</p>
			</div>
		</div>
		<br/><br/>
		<div class="row text-center">
			<div class="col-lg-6">
				<?php
				$attr = array(
					 'title'	 => 'Wordpress Development',
					 'src'		 => 'includes/images/content/api.png',
					 'class'	 => 'img-responsive'
				);

				echo img($attr);
				?>
				<h3>API Integration</h3>
				<p>Having a good understanding process of e-commerce and integrating API for payment, shipping and mailers</p>
			</div>
			<div class="col-lg-6">
				<?php
				$attr = array(
					 'title'	 => 'Wordpress Development',
					 'src'		 => 'includes/images/content/maintenance.png',
					 'class'	 => 'img-responsive'
				);

				echo img($attr);
				?>
				<h3>Maintenance</h3>
				<p>Supporting website content update, hosting & email setup, and debug web applications. </p>
			</div>
		</div>
	</div>
</section>
<hr>
<!--<section id="portfolio">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h1>Portfolio</h1>
				<div class="subtitle-underline underline-align-center underline-orange"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4">
				<a data-toggle="modal" href="#pm-1">
<?php
$attr = array(
	 'title'	 => 'App Development',
	 'src'		 => 'includes/images/content/sample-webapp-dev.png',
	 'class'	 => 'img-responsive'
);

echo img($attr);
?>
				</a>

			</div>
			<div class="col-lg-4">
<?php
$attr = array(
	 'title'	 => 'App Development',
	 'src'		 => 'includes/images/content/sample-webapp-dev.png',
	 'class'	 => 'img-responsive'
);

echo img($attr);
?>
			</div>
			<div class="col-lg-4">
<?php
$attr = array(
	 'title'	 => 'App Development',
	 'src'		 => 'includes/images/content/sample-webapp-dev.png',
	 'class'	 => 'img-responsive'
);

echo img($attr);
?>
			</div>
		</div>
	</div>
</section>
<hr>-->
<section id="about">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h1>About Me</h1>
				<div class="subtitle-underline underline-align-center underline-orange"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<p>I was born in Korea, and currently live in Edmonton, AB Canada. I have a Bachelor’s degree in Computer Science from Thompson Rivers University, and presently work in Edmonton as a PHP Web Application Developer. Mostly I use Codeigniter PHP Framework and have done various projects, websites, web applications, and API integrations. My personality is introverted and shy, but I would like to meet new people and talk. If you would like to know more about me, contact to me. You are always welcome.<br/><br/>
				</p>
				<h3 class="text-center">
					I love programming and believe that I can do everything.
				</h3>
			</div>
		</div>
	</div>
</section>

<section id="contact" class="orange">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h1>Contact</h1>
				<div class="subtitle-underline underline-align-center underline-grey"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<p>
					Send me a message via email. You can any questions or I can help you building a website for free.<br/>
					I'll try my best to get back to you as soon as possible.<br/>
					Have a good day :) <br/><br/>
					Email: jongyulc@gmail.com
				</p>
			</div>
			<div class="col-lg-6">
				<div class="alert alert-success" id="success-alert">
					<strong>Success! </strong>
					Email has been sent.
				</div>

				<form id="contactForm" name="contactForm">
					<div class="row control-group">
						<div class="form-group col-xs-12 floating-label-form-group controls">
							<label>Name</label>
							<input type="text" class="form-control" placeholder="Name" id="name" name="name" required data-validation-required-message="Please enter your name.">
							<p class="help-block text-danger"></p>
						</div>
					</div>
					<div class="row control-group">
						<div class="form-group col-xs-12 floating-label-form-group controls">
							<label>Email Address</label>
							<input type="email" class="form-control" placeholder="Email Address" id="email" name="email" required data-validation-required-message="Please enter your email address.">
							<p class="help-block text-danger"></p>
						</div>
					</div>
					<div class="row control-group">
						<div class="form-group col-xs-12 floating-label-form-group controls">
							<label>Message</label>
							<textarea rows="5" class="form-control" placeholder="Message" id="message" name="message" required data-validation-required-message="Please enter a message."></textarea>
							<p class="help-block text-danger"></p>
						</div>
					</div>
					<br>
					<div id="success"></div>
					<div class="row">
						<div class="form-group col-xs-12">
							<button type="submit" class="btn btn-contact btn-lg">Send</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<!--<div aria-hidden="true" role="dialog" tabindex="-1" id="pm-1" class="portfolio-modal modal fade">
	<div class="modal-content">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="modal-body">

						<h2>Helloship</h2>

						<button data-dismiss="modal" class="btn btn-primary" type="button"><i class="fa fa-times"></i> Close</button>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>-->