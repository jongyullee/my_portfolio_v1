<!-- layout styles -->

<?php
$link_css = array(
	 'href' => 'includes/css/bootstrap.min.css',
	 'rel'	 => 'stylesheet',
	 'type' => 'text/css'
);

echo link_tag($link_css);
echo "\n";

$link_css = array(
	 'href' => 'includes/css/custom.css',
	 'rel'	 => 'stylesheet',
	 'type' => 'text/css'
);

echo link_tag($link_css);
echo "\n";

$link_css = array(
	 'href' => 'includes/css/custom_animation.css',
	 'rel'	 => 'stylesheet',
	 'type' => 'text/css'
);

echo link_tag($link_css);
echo "\n";
?>