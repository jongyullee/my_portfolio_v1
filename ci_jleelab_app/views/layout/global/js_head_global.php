<script src="<?php echo base_url(); ?>includes/js/jquery_1_11_3.js"></script>
<script src="<?php echo base_url(); ?>includes/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>includes/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>includes/js/classie.js"></script>
<script src="<?php echo base_url(); ?>includes/js/cbpAnimatedHeader.js"></script>
<script src="<?php echo base_url(); ?>includes/js/queryloader2.js"></script>
<script src="<?php echo base_url(); ?>includes/js/script.js"></script>

<script>
   (function (i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function () {
         (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date();
      a = s.createElement(o),
              m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
   })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

   ga('create', 'UA-75976010-1', 'auto');
   ga('send', 'pageview');

</script>

<!--[if lt IE 9]>
	<script src="<?php echo base_url(); ?>includes/js/html5shiv.min.js"></script>
<![endif]-->

