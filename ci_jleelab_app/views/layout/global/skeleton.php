<?php echo $this->config->item('template_doctype'); ?>
<html>
	<head>
		<?php echo $this->config->item('template_meta_http_equiv'); ?>

		<title><?php echo $browser_title . $this->config->item('browser_title_base'); ?></title>
		<?php echo $this->config->item('template_robots'); ?>

		<?php echo $this->config->item('template_viewport'); ?>

		<?php echo $this->config->item('template_favicon'); ?>

		<?php $this->load->view('layout/global/css'); ?>

		<?php echo $_styles; ?>

		<?php $this->load->view('layout/global/js_head_global'); ?>

		<?php echo $_scripts; ?>

	</head>

	<body>
		<header id="top">
			<?php $this->load->view('layout/global/nav'); ?>
			<?php $this->load->view('layout/global/header'); ?>
		</header>

		<main>
			<?php echo $content; ?>
		</main>

		<footer>
			<?php $this->load->view('layout/global/footer'); ?>
		</footer>

	</body>
</html>