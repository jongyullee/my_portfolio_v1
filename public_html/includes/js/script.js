$(document).ready(function () {

   /*
    *    Loading progress bar before displaying website
    */

   $("body").queryLoader2({
      barColor: "#efefef",
      backgroundColor: "#111",
      percentage: true,
      barHeight: 1,
      minimumTime: 200,
      fadeOutTime: 1000
   });


   /*
    *    Page scroll Navigation
    */

   var nav_offset;

   if ($(document).width() < 768) {
      nav_offset = 52;
   }
   else {
      nav_offset = 79;
   }

   $('body').on('click', '.page-scroll a', function (event) {
      var $anchor = $(this);
      $('html, body').stop().animate({
         scrollTop: $($anchor.attr('href')).offset().top - nav_offset
      }, 1500, 'easeInOutExpo');
      event.preventDefault();
   });

   $('body').scrollspy({
      target: '.navbar-fixed-top',
      offset: nav_offset + 1
   });

   $('.navbar-collapse ul li a').click(function () {
      $('.navbar-toggle:visible').click();
   });


   /*
    *    page animation control
    */

   $('section').each(function () {
      animation_control();
   });

   $(window).scroll(function () {
      animation_control();
   });

   function animation_control() {
      
      $('section').each(function () {
         var imagePos = $(this).offset().top;
         var topOfWindow = $(window).scrollTop();

         if (imagePos < topOfWindow + 800) {
            $(this).find('h3').addClass("scene_element ani-fadein-up ani-duration-1");
            $(this).find('hr').addClass("scene_element ani-fadein-up ani-duration-05");
            $(this).find('p').addClass("scene_element ani-fadein-up ani-duration-1");
            $(this).find('img').addClass("scene_element ani-fadein-up ani-duration-1");
            $(this).find('#contactForm').addClass("scene_element ani-fadein-up ani-duration-1");
         }
      });
      
   }


   /*
    *    Ajax contact form
    */

   $("#success-alert").hide();

   $("#contactForm").submit(function (e)
   {
      var postData = $(this).serializeArray();
      var formURL = 'contact';

      e.preventDefault();

      $.ajax(
              {
                 url: formURL,
                 type: "POST",
                 data: postData,
                 success: function (data, textStatus, jqXHR)
                 {
                    $("#success-alert")
                            .fadeIn(500)
                            .delay(2000)
                            .fadeOut(500, function () {
                               $("#success-alert").hide();
                            });
                                                        
                 },
                 error: function (jqXHR, textStatus, errorThrown)
                 {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);

                 }
              });

   });

});


